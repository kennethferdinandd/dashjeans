from django import forms
from ..models import Review

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ["deskripsi"]
        widgets = {
            "deskripsi": forms.Textarea(attrs={"class" : "custom-textarea", "required" : False})
        }