from django.shortcuts import render, get_object_or_404, redirect
from ..booking.views import is_field_booked
from django.contrib.auth.decorators import login_required
from django.db.models import Avg
from django.http import HttpResponse, JsonResponse
from ..models import Lapangan, Review
from .forms import ReviewForm
from django.core import serializers
import datetime
import json
from django.views.decorators.csrf import csrf_exempt


@login_required(login_url="/auth/login/")
@csrf_exempt
def lapangan_detail(request, pk):
    lapangan = Lapangan.objects.filter(pk=pk)
    if lapangan.exists():
        lapangan = lapangan.get(pk=pk)
        lapangan.deskripsi = lapangan.deskripsi.strip()
        reviews = Review.objects.filter(lapangan__id=pk)
        if request.user.role == "pengguna":
            extracted_json = is_field_booked(request, pk).content.decode('utf-8')
            is_booked = json.loads(extracted_json).get('is_field_booked')
        else:
            is_booked = False
        
        no_description = all(review.deskripsi == "" for review in reviews)
        avg_rating = reviews.aggregate(Avg('rating'))['rating__avg'] or 0.0
        review_count = reviews.count()
        return render(request, 'detail_review/lapangan_detail.html', {'lapangan': lapangan, 
                                                                    'fromOwnedPage': False, 
                                                                    'reviews': reviews,
                                                                    'noDescription': no_description,
                                                                    'isBooked': is_booked,
                                                                    'avgRating': round(avg_rating, 2),
                                                                    'reviewCount': review_count,
                                                                    'reviewForm': ReviewForm
                                                                    })
    else:
        return redirect('home')
    
@login_required(login_url="/auth/login/")
@csrf_exempt
def create_review(request):
    if request.user.role == "pengguna":
        reviewForm = ReviewForm(request.POST or None)
        if request.method == 'POST' and reviewForm.is_valid():
            formData = request.POST.copy()
            lapangan_id = formData.pop('lapangan_id')[0]
            rating = formData.pop('rating')[0]
            isSkipped = formData.pop('is_skipped')[0]
            # Convert to boolean
            if isSkipped == "true":
                isSkipped = True
            else:
                isSkipped = False

            if not isSkipped:
                try:
                    description = reviewForm.cleaned_data.get("deskripsi", "").strip()
                    if description == "":
                        raise ValueError("Deskripsi tidak boleh kosong.")
                    elif len(description) > 2500:
                        raise ValueError("Deskripsi tidak boleh melebihi 2500 karakter.")
                    else:
                        Review(
                            pengguna = request.user,
                            lapangan = Lapangan.objects.get(id=lapangan_id),
                            rating = rating,
                            deskripsi = description,
                            tanggal = datetime.date.today(),
                        ).save()
                        return JsonResponse({'status': 'success'})
                except ValueError as e:
                    error_message = str(e)
                    return JsonResponse({'status': 'error', 'message': error_message})

            else:
                Review(
                    pengguna = request.user,
                    lapangan = Lapangan.objects.get(id=lapangan_id),
                    rating = rating,
                    deskripsi = "",
                    tanggal = datetime.date.today(),
                ).save()
            return JsonResponse({'status': 'success'})
        else:
            form = ReviewForm()
    else:
        return HttpResponse("You dont have access to view this page", status=403)

@login_required(login_url="/auth/login/")
@csrf_exempt
def show_review(request):
    return HttpResponse(
        serializers.serialize("json", Review.objects.all()), content_type="application/json"
    )
