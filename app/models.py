from unittest.util import _MAX_LENGTH
from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin,
    BaseUserManager,
    PermissionsMixin,
    Group,
    Permission,
)
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings
from django.core.exceptions import ValidationError
import os


def rename_bukti_pembayaran(instance, filename):
    upload_to = "bukti_pembayaran/"
    ext = filename.split(".")[-1]  # Get the file extension
    filename = f"{instance.user.username}_{instance.id}.{ext}"  # Customize the filename
    return os.path.join(upload_to, filename)


class CustomUserManager(BaseUserManager):
    def create_user(
        self, no_telp, email, nama, role, username, password=None, foto=None
    ):
        if not no_telp:
            raise ValueError("Tidak ada no telp yang dimasukkan")
        if not email:
            raise ValueError("Tidak Ada Email yang Dimasukkan")
        if not password:
            raise ValueError("Tidak Ada Password yang Dimasukkan")
        if not username:
            raise ValueError("Tidak ada username yang dimasukkan")

        email = self.normalize_email(email)
        user = self.model(
            email=email,
            nama=nama,
            role=role,
            username=username,
            foto=foto,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self, no_telp, email, nama, username, password=None, foto=None):
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            nama=nama,
            no_telp=no_telp,
            username=username,
            foto=foto,
            role='pihak_penyedia_lapangan',  # Assuming you want the superuser to have this role
        )
        user.is_superuser = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    ROLE_CHOICES = [
        ("pengguna", "Pengguna"),
        ("pihak_penyedia_lapangan", "Pihak Penyedia Lapangan"),
    ]

    no_telp = models.CharField(max_length=15, unique=True)
    email = models.EmailField(unique=True)
    nama = models.CharField(max_length=255)
    role = models.CharField(max_length=50, choices=ROLE_CHOICES)
    username = models.CharField(max_length=50, unique=True)
    password = models.CharField(max_length=128)
    foto = models.ImageField(upload_to="user_photos/", null=True, blank=True)
    groups = models.ManyToManyField(Group, related_name="custom_users", blank=True)
    user_permissions = models.ManyToManyField(
        Permission, related_name="custom_users", blank=True
    )
    is_staff = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["no_telp", "nama", "password", "email"]


class Lapangan(models.Model):
    nama = models.CharField(max_length=255)
    deskripsi = models.TextField()
    alamat = models.TextField()
    harga_perjam = models.IntegerField()
    no_telp = models.CharField(max_length=15)
    foto = models.ImageField(upload_to="sportfields_photos/", null=True, blank=True)
    pemilik = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    jadwal = models.ManyToManyField("Jadwal", related_name="lapangan_jadwal")
    booking = models.ManyToManyField("Booking", related_name="lapangan_booking")
    isDipesan = models.BooleanField(default=False)

    def __str__(self):
        return self.nama


class Jadwal(models.Model):
    STATUS = [
        ("tersedia", "Tersedia"),
        ("dipesan", "Sedang Dipesan"),
        ("terpesan", "Terpesan"),
    ]
    lapangan = models.ForeignKey(
        Lapangan, on_delete=models.CASCADE, related_name="jadwals"
    )
    tanggal = models.DateField()
    jam_mulai = models.TimeField()
    jam_selesai = models.TimeField()
    status = models.CharField(max_length=50, choices=STATUS)

    def __str__(self):
        return f"{self.lapangan.nama} - {self.tanggal} - {self.jam_mulai} - {self.jam_selesai}"


class Booking(models.Model):
    STATUS = [
        ("menunggu", "Menunggu Verifikasi"),
        ("terverifikasi", "Terverifikasi"),
        ("gagal", "Gagal"),
    ]
    lapangan = models.ForeignKey(
        Lapangan, on_delete=models.CASCADE, related_name="bookings"
    )
    jadwals = models.ManyToManyField("Jadwal", related_name="bookings")
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    status = models.CharField(max_length=50, choices=STATUS)
    waktu_pesan = models.DateTimeField(auto_now_add=True)
    jumlah_jam = models.IntegerField()
    total_harga = models.IntegerField()
    bukti_pembayaran = models.ImageField(upload_to=rename_bukti_pembayaran)

    def __str__(self):
        return f"{self.user.nama} - {self.lapangan.nama} - {self.waktu_pesan}"

class Review(models.Model):
    pengguna = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name="pengguna")
    lapangan = models.ForeignKey(Lapangan, on_delete=models.CASCADE, related_name="lapangan")
    rating = models.IntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)],
    )
    deskripsi = models.TextField(max_length=2500, null=True, blank=True)
    tanggal = models.DateField()