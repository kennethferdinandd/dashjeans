from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path("booking/", include("app.booking.urls"), name="booking"),
    path("auth/", include("app.authentication.urls"), name="authentication"),
    path("ppl/lapangan/", include("app.ppl_lapangan.urls"), name="ppl_lapangan"),
    path("lapangan/", include("app.detail_review.urls"), name="lapangan"),
    path('media/bukti_pembayaran/<str:file_name>/', bukti_pembayaran_page, name='bukti_pembayaran_page'),
    path("", index, name="home"),
]
