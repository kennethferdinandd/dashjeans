from django import forms
from ..models import Booking, Jadwal


class BookingForm(forms.ModelForm):
    jadwals = forms.ModelMultipleChoiceField(
        queryset=Jadwal.objects.none(),
        widget=forms.CheckboxSelectMultiple,
    )

    def __init__(self, lapangan_id, *args, **kwargs):
        super(BookingForm, self).__init__(*args, **kwargs)

        # Filter the jadwals queryset based on the lapangan_id
        self.fields["jadwals"].queryset = Jadwal.objects.filter(
            lapangan_id=lapangan_id,
            status="tersedia",
        ).order_by("tanggal", "jam_mulai")

    class Meta:
        model = Booking
        fields = ["jadwals", "bukti_pembayaran"]
