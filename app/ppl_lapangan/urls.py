from django.urls import path, include
from .views import *

urlpatterns = [
    path("", get_owned_lapangan, name="get_owned_lapangan"),
    path("register/", register_lapangan, name="register_lapangan"),
    path("register/jadwal/<int:lapangan_id>", create_jadwal, name="create_jadwal"),
    path('update/<int:pk>/', update_lapangan, name='update_lapangan'),
    path('detail/<int:pk>/', lapangan_detail, name='lapangan_detail_owned'),
    path('view_history/', view_history, name='view_history')
]